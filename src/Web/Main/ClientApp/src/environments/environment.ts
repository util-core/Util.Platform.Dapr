/**
 * 开发环境配置
 */
export const environment = {
    /**
     * 是否生产环境
     */
    production: false,
    /**
     * 是否使用带 # 的 url
     */
    useHash: false,
    /**
     * 认证服务器地址
     */
    identityUrl: "http://localhost:30100",
    /**
     * Api端点地址
     */
    apiEndpointUrl: "http://localhost:30101"
};